
// Structs Demo

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum DoorState
{
	OPEN, CLOSING, CLOSED, OPENING
};

struct Door
{
	DoorState state;
	bool isLocked;
};

struct Student
{
	string firstName;
	string lastName;
	float gpa;
};

int main()
{
	Student s1;
	s1.firstName = "Brian";
	s1.lastName = "Foote";
	s1.gpa = 3.7f;



	_getch();
	return 0;
}